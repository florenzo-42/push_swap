# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/02/01 17:36:55 by florenzo          #+#    #+#              #
#    Updated: 2018/02/01 17:36:55 by florenzo         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

LIBFTPATH = ./libft
LIBFT = $(LIBFTPATH)/libft.a

CFLAGS = -Wall -Wextra -Werror -I. -I$(LIBFTPATH) -Iinc

PUSHSWAP = push_swap
PUSHSWAPSRC = push_swap.c \
			  algorithms/*.c \
			  compare_algorithms.c \
			  n_xx.c \
			  new_instruction.c \
			  ps_px.c \
			  ps_rrx.c \
			  ps_rx.c \
			  ps_setup.c \
			  ps_sort.c \
			  ps_sx.c \
			  seq_add.c \
			  seq_free.c \
			  seq_optimize.c \
			  seq_print.c
PUSHSWAPSRC := $(addprefix src/push_swap/, $(PUSHSWAPSRC))
PUSHSWAPOBJ = $(PUSHSWAPSRC:%.c=%.o)

CHECKER = checker
CHECKERSRC = checker.c \
			 chk_args.c \
			 chk_handle_input.c \
			 chk_messages.c \
			 chk_px.c \
			 chk_rrx.c \
			 chk_rx.c \
			 chk_setup.c \
			 chk_sx.c \
			 get_flags.c
CHECKERSRC := $(addprefix src/checker/, $(CHECKERSRC))
CHECKEROBJ = $(CHECKERSRC:%.c=%.o)

SRC = build_stack.c \
	  iarr_bsearch.c \
	  iarr_check_doubles.c \
	  iarr_copy.c \
	  iarr_free.c \
	  iarr_new.c \
	  is_stack_sorted.c \
	  new_node.c \
	  params_to_iarray.c \
	  show_stacks.c \
	  node_copy.c \
	  stack_add_end.c \
	  stack_add_first.c \
	  stack_copy.c \
	  stack_free.c \
	  stack_init.c \
	  stack_print.c \
	  stack_remove.c \
	  stack_reverse_rotate.c \
	  stack_rotate.c \
	  stack_swap.c
SRC := $(addprefix src/common/, $(SRC))
OBJ = $(SRC:%.c=%.o)

NAME = $(LIBFT) $(CHECKER) $(PUSHSWAP)

all: $(NAME)

$(OBJ) : ./inc/common.h
$(CHECKEROBJ) : ./inc/common.h ./inc/checker.h
$(PUSHSWAPOBJ) : ./inc/common.h ./inc/push_swap.h

$(LIBFT) :
	make -C $(LIBFTPATH)

$(CHECKER): $(LIBFT) $(OBJ) $(CHECKEROBJ)
	make -C $(LIBFTPATH)
	$(CC) $(CFLAGS) $(OBJ) $(CHECKEROBJ) $(LIBFT) -o $(CHECKER)

$(PUSHSWAP): $(LIBFT) $(OBJ) $(PUSHSWAPOBJ)
	make -C $(LIBFTPATH)
	$(CC) $(CFLAGS) $(OBJ) $(PUSHSWAPOBJ) $(LIBFT) -o $(PUSHSWAP)

clean:
	make clean -C $(LIBFTPATH)
	rm -f $(OBJ) $(PUSHSWAPOBJ) $(CHECKEROBJ)

fclean: clean
	make fclean -C $(LIBFTPATH)
	rm -f $(NAME)

re: fclean all

.PHONY: all clean fclean re

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_rrx.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:39:00 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:39:00 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				core_rrx(int flag, t_stack *a, t_stack *b)
{
	static t_stack	*aptr;
	static t_stack	*bptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		return ;
	}
	if (FLAG_A & flag)
		stack_reverse_rotate(aptr);
	if (FLAG_B & flag)
		stack_reverse_rotate(bptr);
}

void				rra(void)
{
	core_rrx(FLAG_A, NULL, NULL);
}

void				rrb(void)
{
	core_rrx(FLAG_B, NULL, NULL);
}

void				rrr(void)
{
	core_rrx(FLAG_A | FLAG_B, NULL, NULL);
}

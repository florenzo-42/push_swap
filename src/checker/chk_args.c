/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_args.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 15:10:49 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/08 15:10:49 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void			set_color(char **option, char *str)
{
	if (ft_strequ(str, "black"))
		*option = "\033[30m";
	else if (ft_strequ(str, "red"))
		*option = "\033[31m";
	else if (ft_strequ(str, "green"))
		*option = "\033[32m";
	else if (ft_strequ(str, "yellow"))
		*option = "\033[33m";
	else if (ft_strequ(str, "blue"))
		*option = "\033[34m";
	else if (ft_strequ(str, "magenta"))
		*option = "\033[35m";
	else if (ft_strequ(str, "cyan"))
		*option = "\033[36m";
	else if (ft_strequ(str, "white"))
		*option = "\033[37m";
	else
		ft_usage();
}

void				long_arg(char *arg, t_flags *flags)
{
	arg += 2;
	if (ft_strequ(arg, "help"))
		ft_help();
	else if (ft_strequ(arg, "verbose"))
		flags->verbose = TRUE;
	else if (ft_strequ(arg, "color") || ft_strequ(arg, "colors"))
		flags->colors = TRUE;
	else if (ft_strequ(arg, "count") || ft_strequ(arg, "counter"))
		flags->counter = TRUE;
	else if (ft_strequ(arg, "repeat"))
		flags->repeat = TRUE;
	else if (ft_strnequ(arg, "acolor=", 7))
		set_color(&(flags->acolor), arg + 7);
	else if (ft_strnequ(arg, "bcolor=", 7))
		set_color(&(flags->bcolor), arg + 7);
	else if (ft_strequ(arg, "index"))
		flags->index = TRUE;
	else
		ft_usage();
}

static void			understand_char(char c, t_flags *flags)
{
	if ('h' == c)
		ft_help();
	else if ('v' == c)
		flags->verbose = TRUE;
	else if ('c' == c)
	{
		flags->verbose = TRUE;
		flags->colors = TRUE;
	}
	else if ('n' == c)
		flags->counter = TRUE;
	else if ('i' == c)
		flags->index = TRUE;
	else if ('r' == c)
		flags->repeat = TRUE;
	else
		ft_usage();
}

void				short_arg(char *str, t_flags *flags)
{
	str += 1;
	if ('\0' == *str)
		ft_usage();
	while ('\0' != *str)
	{
		understand_char(*str, flags);
		str += 1;
	}
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 20:37:51 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/07 20:37:51 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

static void			show_result(t_stack *a, t_stack *b)
{
	if (0 == b->len && is_stack_sorted(a))
		ft_printf("OK\n");
	else
		ft_printf("KO\n");
}

static void			show_count(t_flags *flags)
{
	if (flags->counter)
		ft_printf("Played %i move%s\n", flags->count,
				flags->count > 1 ? "s" : "");
}

static void			chk_read_stdin(t_stack *a, t_stack *b,
		t_flags *flags)
{
	char			*str;

	while (0 < get_next_line(0, &str))
	{
		if (flags->repeat)
			ft_printf("%s\n", str);
		if (chk_handle_input(str))
		{
			ft_dprintf(2, "Error\n");
			flags->error = TRUE;
			free(str);
			break ;
		}
		show_stacks(a, b, flags);
		flags->count += 1;
		free(str);
	}
}

static void			chk_init_flags(t_flags *flags)
{
	flags->verbose = FALSE;
	flags->colors = FALSE;
	flags->error = FALSE;
	flags->counter = FALSE;
	flags->index = FALSE;
	flags->repeat = FALSE;
	flags->count = 0;
	flags->acolor = "";
	flags->bcolor = "";
}

int					main(int ac, char **av)
{
	t_stack			a;
	t_stack			b;
	t_flags			flags;

	if (ac < 2)
		ft_usage();
	chk_init_flags(&flags);
	get_flags(&ac, &av, &flags);
	chk_correct_flags(&flags);
	if (0 >= build_stack(&a, params_to_iarray(ac, av)))
	{
		ft_dprintf(2, "Error\n");
		return (-1);
	}
	stacks_setup(&a, &b);
	show_stacks(&a, &b, &flags);
	chk_read_stdin(&a, &b, &flags);
	if (!flags.error)
		show_result(&a, &b);
	show_count(&flags);
	stack_free(&a);
	stack_free(&b);
	return (0);
}

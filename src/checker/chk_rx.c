/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_rx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:39:03 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:39:03 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				core_rx(int flag, t_stack *a, t_stack *b)
{
	static t_stack	*aptr;
	static t_stack	*bptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		return ;
	}
	if (FLAG_A & flag)
		stack_rotate(aptr);
	if (FLAG_B & flag)
		stack_rotate(bptr);
}

void				ra(void)
{
	core_rx(FLAG_A, NULL, NULL);
}

void				rb(void)
{
	core_rx(FLAG_B, NULL, NULL);
}

void				rr(void)
{
	core_rx(FLAG_A | FLAG_B, NULL, NULL);
}

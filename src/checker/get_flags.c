/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_flags.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 20:41:35 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/07 20:41:35 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				get_flags(int *ac, char ***av, t_flags *flags)
{
	int				pos;

	pos = 1;
	while (pos < *ac)
	{
		if ((*av)[pos][0] == '-')
		{
			if ((*av)[pos][1] == '-' || (*av)[pos][1] == '=')
				long_arg(*((*av) + pos), flags);
			else if (!ft_isdigit((*av)[pos][1]))
				short_arg(*((*av) + pos), flags);
			else
				break ;
		}
		else
			break ;
		pos += 1;
	}
	pos -= 1;
	*ac -= pos;
	*av += pos;
}

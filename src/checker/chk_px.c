/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_px.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:39:09 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:39:09 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				core_px(int flag, t_stack *a, t_stack *b)
{
	static t_stack	*aptr;
	static t_stack	*bptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
	}
	else if (FLAG_A & flag)
	{
		stack_add_first(aptr, stack_remove(bptr));
	}
	else
	{
		stack_add_first(bptr, stack_remove(aptr));
	}
}

void				pa(void)
{
	core_px(FLAG_A, NULL, NULL);
}

void				pb(void)
{
	core_px(FLAG_B, NULL, NULL);
}

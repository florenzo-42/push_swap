/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_messages.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 17:08:56 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/08 17:08:56 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				ft_help(void)
{
	ft_printf("Usage : checker [options] args\nSupported options :\n"
			"    -r --repeat    Repeat each move entered.\n"
			"    -v --verbose   Show stacks after each move.\n"
			"    -i --index     Show index of each number (implies -v).\n"
			"    -c --colors    Enable colored output (implies -v).\n"
			"    -n --count     Display final number of moves at the end.\n"
			"    -h --help      Show this help message and exit.\n"
			"\nNOTES :\n"
			"    If enabled colors default to red and cyan\n"
			"    To specify them use --(a/b)color=[color]\n"
			"    The 8 ANSI colors are accepted : black, red, green, yellow,"
			"blue, magenta, cyan and white\n"
			"        For example : \"--acolor=yellow\"\n"
			"    -- can be replaced with -= (QWERTY lazyness)\n");
	exit(0);
}

void				ft_usage(void)
{
	ft_dprintf(2, "Error\n");
	ft_printf("Usage : checker [options] args\n"
			"For more info on supported flags rerun with checker --help\n");
	exit(1);
}

void				chk_correct_flags(t_flags *flags)
{
	if (flags->index)
		flags->verbose = TRUE;
	if ('\0' != *(flags->acolor) || '\0' != *(flags->bcolor))
		flags->colors = TRUE;
	if (flags->colors)
	{
		flags->verbose = TRUE;
		if ('\0' == *(flags->acolor) && '\0' == *(flags->bcolor))
		{
			flags->acolor = "\033[31m";
			flags->bcolor = "\033[36m";
		}
	}
}

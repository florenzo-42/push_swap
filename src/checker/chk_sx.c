/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_sx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:38:58 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:38:58 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				core_sx(int flag, t_stack *a, t_stack *b)
{
	static t_stack	*aptr;
	static t_stack	*bptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		return ;
	}
	if (FLAG_A & flag)
		stack_swap(aptr);
	if (FLAG_B & flag)
		stack_swap(bptr);
}

void				sa(void)
{
	core_sx(FLAG_A, NULL, NULL);
}

void				sb(void)
{
	core_sx(FLAG_B, NULL, NULL);
}

void				ss(void)
{
	core_sx(FLAG_A | FLAG_B, NULL, NULL);
}

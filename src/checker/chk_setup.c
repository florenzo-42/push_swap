/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_setup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 19:58:05 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 19:58:05 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				stacks_setup(t_stack *a, t_stack *b)
{
	b->len = 0;
	b->first = NULL;
	core_sx(SETUP, a, b);
	core_px(SETUP, a, b);
	core_rx(SETUP, a, b);
	core_rrx(SETUP, a, b);
}

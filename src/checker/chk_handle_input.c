/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   chk_handle_input.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 15:12:58 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/08 15:12:58 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

int					chk_handle_input(char *str)
{
	if (ft_strequ(str, "pa"))
		pa();
	else if (ft_strequ(str, "pb"))
		pb();
	else if (ft_strequ(str, "ra"))
		ra();
	else if (ft_strequ(str, "rb"))
		rb();
	else if (ft_strequ(str, "rr"))
		rr();
	else if (ft_strequ(str, "rra"))
		rra();
	else if (ft_strequ(str, "rrb"))
		rrb();
	else if (ft_strequ(str, "rrr"))
		rrr();
	else if (ft_strequ(str, "sa"))
		sa();
	else if (ft_strequ(str, "sb"))
		sb();
	else if (ft_strequ(str, "ss"))
		ss();
	else
		return (1);
	return (0);
}

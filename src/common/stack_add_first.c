/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_add_first.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 20:30:23 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 20:30:23 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node			*stack_add_first(t_stack *stack, t_node *node)
{
	t_node		*ret;

	ret = stack_add_end(stack, node);
	if (NULL == ret)
		return (NULL);
	stack->first = ret;
	return (ret);
}

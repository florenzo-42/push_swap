/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   is_stack_sorted.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/08 16:54:45 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/08 16:54:45 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

int				is_stack_sorted(t_stack *stack)
{
	t_node	*current;
	int			pos;

	if (NULL == stack || 1 >= stack->len)
		return (TRUE);
	pos = 1;
	current = stack->first;
	while (pos < stack->len)
	{
		if (current->value > current->next->value)
			return (FALSE);
		current = current->next;
		pos += 1;
	}
	return (TRUE);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_free.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 18:52:07 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 18:52:07 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void				stack_free(t_stack *stack)
{
	t_node	*tmp;

	if (NULL == stack || 0 >= stack->len)
		return ;
	while (stack->len > 0)
	{
		tmp = stack->first;
		stack->first = stack->first->next;
		free(tmp);
		stack->len -= 1;
	}
	stack->first = NULL;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   params_to_iarray.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:31:06 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:31:06 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

static int		is_invalid_num(char const *str)
{
	int		pos;

	pos = 0;
	if (str[pos] == '-' || str[pos] == '+')
		pos += 1;
	while (pos < 13 && ft_isdigit(str[pos]))
		pos += 1;
	if (0 == pos || '\0' != str[pos])
		return (1);
	return (0);
}

static int		bad_number(t_iarray *iarr, char const *str, int const index)
{
	intmax_t	res;

	res = ft_atoim(str);
	if (res > (intmax_t)INT_MAX || res < (intmax_t)INT_MIN)
		return (1);
	(iarr->array)[index] = (int)res;
	return (0);
}

t_iarray		*params_to_iarray(int ac, char **av)
{
	t_iarray	*new;
	int			pos;

	if (1 >= ac || NULL == (new = iarr_new(ac - 1)))
		return (NULL);
	pos = 1;
	while (pos < ac)
	{
		if (is_invalid_num(av[pos]) || bad_number(new, av[pos], pos - 1))
		{
			iarr_free(new);
			return (NULL);
		}
		pos += 1;
	}
	return (new);
}

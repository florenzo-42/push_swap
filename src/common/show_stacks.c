/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_stacks.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 19:32:11 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 19:32:11 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "checker.h"

void				show_stacks(t_stack *a, t_stack *b,
		t_flags *flags)
{
	if (flags->verbose)
	{
		stack_print(a, 'a',
				flags->colors ? flags->acolor : "", flags->index);
		stack_print(b, 'b',
				flags->colors ? flags->bcolor : "", flags->index);
	}
}

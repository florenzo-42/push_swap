/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iarr_bsearch.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 18:11:24 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 18:11:24 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

int				iarr_bsearch(int n, t_iarray *iarr, int growing)
{
	int			low;
	int			high;
	int			ret;
	int			*array;

	low = 0;
	high = iarr->size - 1;
	array = iarr->array;
	while (n != array[(ret = ((low + high) / 2))] && high != low)
	{
		if ((array[ret] > n) ^ growing)
			low = ret + 1;
		else
			high = ret;
	}
	return (ret);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iarr_copy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:30:58 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:30:58 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_iarray	*iarr_copy(t_iarray *iarr)
{
	t_iarray	*copy;

	if (NULL == iarr || NULL == (copy = iarr_new(iarr->size)))
		return (NULL);
	ft_memmove(copy->array, iarr->array, iarr->size * sizeof(int));
	return (copy);
}

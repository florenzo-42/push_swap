/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   build_stack.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:31:02 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:31:02 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

static void	add_ranks_to_nodes(t_stack *stack, t_iarray *sorted)
{
	t_node	*current;
	int			pos;

	pos = 0;
	current = stack->first;
	while (pos < stack->len)
	{
		current->rank = iarr_bsearch(current->value, sorted, 1);
		current = current->next;
		pos += 1;
	}
}

/*
** Since we push at the front we have to iterate from the end.
*/

static int	push_all_nodes(t_stack *stack, t_iarray *iarr)
{
	while (iarr->size > 0)
	{
		iarr->size -= 1;
		if (NULL == stack_add_first(stack,
					new_node(iarr->array[iarr->size])))
		{
			stack_free(stack);
			iarr_free(iarr);
			return (1);
		}
	}
	return (0);
}

int			build_stack(t_stack *stack, t_iarray *iarr)
{
	t_iarray	*sorted;

	if (NULL == iarr || NULL == (sorted = iarr_check_doubles(iarr)))
	{
		iarr_free(iarr);
		return (0);
	}
	stack->len = 0;
	stack->first = NULL;
	if (push_all_nodes(stack, iarr))
		return (0);
	iarr->size = stack->len;
	add_ranks_to_nodes(stack, sorted);
	iarr_free(iarr);
	iarr_free(sorted);
	return (stack->len);
}

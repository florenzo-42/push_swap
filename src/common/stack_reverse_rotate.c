/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_reverse_rotate.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 13:18:29 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/07 13:18:29 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void				stack_reverse_rotate(t_stack *stack)
{
	if (NULL == stack || 1 >= stack->len)
		return ;
	stack->first = stack->first->prev;
}

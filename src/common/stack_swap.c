/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_swap.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/07 13:21:44 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/07 13:21:44 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void				stack_swap(t_stack *stack)
{
	if (NULL == stack || 1 >= stack->len)
		return ;
	if (2 == stack->len)
	{
		stack->first = stack->first->next;
		return ;
	}
	stack->first->prev->next = stack->first->next;
	stack->first->next->prev = stack->first->prev;
	stack->first->prev = stack->first->next;
	stack->first->next = stack->first->next->next;
	stack->first->next->prev = stack->first;
	stack->first->prev->next = stack->first;
	stack->first = stack->first->prev;
}

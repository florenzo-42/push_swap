/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   node_copy.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 17:45:06 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 17:45:06 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node		*node_copy(t_node *orig)
{
	t_node	*ret;

	if (NULL == (ret = malloc(sizeof(t_node))))
		return (NULL);
	ret->value = orig->value;
	ret->rank = orig->rank;
	return (ret);
}

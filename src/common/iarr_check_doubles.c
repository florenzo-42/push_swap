/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iarr_check_doubles.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:30:46 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:30:46 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

static int	partition(int *array, int size)
{
	int		i;
	int		mid;
	int		tmp;

	mid = 0;
	i = 0;
	while (i < size - 1)
	{
		if (array[i] < array[size - 1])
		{
			tmp = array[i];
			array[i] = array[mid];
			array[mid] = tmp;
			mid += 1;
		}
		i += 1;
	}
	tmp = array[mid];
	array[mid] = array[size - 1];
	array[size - 1] = tmp;
	return (mid);
}

static void	quicksort(int *array, int size)
{
	int		mid;

	if (size < 2)
		return ;
	mid = partition(array, size);
	quicksort(array, mid);
	quicksort(array + mid + 1, size - mid - 1);
}

static int	there_are_duplicate_elements(t_iarray *iarr)
{
	int		pos;

	pos = 0;
	while (pos < iarr->size - 1)
	{
		if (iarr->array[pos] == iarr->array[pos + 1])
			return (1);
		pos += 1;
	}
	return (0);
}

t_iarray	*iarr_check_doubles(t_iarray *iarr)
{
	t_iarray	*sorted;

	if (NULL == (sorted = iarr_copy(iarr)))
		return (NULL);
	quicksort(sorted->array, sorted->size);
	if (there_are_duplicate_elements(sorted))
	{
		iarr_free(sorted);
		return (NULL);
	}
	return (sorted);
}

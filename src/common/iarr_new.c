/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iarr_new.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:31:00 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:31:00 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_iarray		*iarr_new(int const size)
{
	t_iarray	*new;

	if (NULL == (new = malloc(sizeof(t_iarray))))
		return (NULL);
	if (NULL == (new->array = malloc(sizeof(int) * size)))
	{
		free(new);
		return (NULL);
	}
	new->size = size;
	return (new);
}

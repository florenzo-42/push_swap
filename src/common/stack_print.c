/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   swaptack_print.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 16:25:32 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 16:25:32 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void	stack_print(t_stack *stack, char name, char *color, int index)
{
	t_node		*current;
	int				pos;

	ft_dprintf(2, "%sstack %c : ", color, name);
	if (0 == stack->len)
	{
		ft_dprintf(2, "empty stack" RESET "\n");
		return ;
	}
	current = stack->first;
	pos = 0;
	while (pos < stack->len)
	{
		if (index)
			ft_dprintf(2, "(%i, %i) ", current->value, current->rank);
		else
			ft_dprintf(2, "%i ", current->value);
		pos += 1;
		current = current->next;
	}
	ft_dprintf(2, RESET "\n");
}

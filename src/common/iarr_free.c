/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   iarr_free.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:30:56 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:30:56 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

void		iarr_free(t_iarray *iarr)
{
	if (NULL == iarr)
		return ;
	free(iarr->array);
	free(iarr);
}

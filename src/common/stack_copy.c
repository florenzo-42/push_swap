/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_copy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 17:27:48 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 17:27:48 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

int				stack_dup(t_stack *orig, t_stack *new)
{
	t_node	*current;

	new->len = 0;
	current = orig->first;
	while (new->len < orig->len)
	{
		if (NULL == stack_add_end(new, node_copy(current)))
		{
			stack_free(new);
			return (1);
		}
		current = current->next;
	}
	return (0);
}

void			stack_copy(t_stack *orig, t_stack *new)
{
	t_node	*original;
	t_node	*current;

	original = orig->first;
	current = new->first;
	while (original->next != orig->first)
	{
		current->rank = original->rank;
		current->value = original->value;
		original = original->next;
		current = current->next;
	}
	current->rank = original->rank;
	current->value = original->value;
}

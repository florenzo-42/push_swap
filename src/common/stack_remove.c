/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_remove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/06 10:44:46 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/06 10:44:46 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node			*stack_remove(t_stack *stack)
{
	t_node		*node;

	if (NULL == stack || 0 >= stack->len)
		return (NULL);
	node = stack->first;
	if (1 == stack->len)
	{
		stack->len = 0;
		stack->first = NULL;
		return (node);
	}
	node->prev->next = node->next;
	node->next->prev = node->prev;
	stack->first = node->next;
	stack->len -= 1;
	return (node);
}

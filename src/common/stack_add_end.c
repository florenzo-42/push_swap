/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_add_end.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 14:01:20 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 14:01:20 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node			*stack_add_end(t_stack *stack, t_node *node)
{
	if (NULL == stack || NULL == node || 0 > stack->len)
		return (NULL);
	if (0 == stack->len)
		return (stack_init(stack, node));
	if (1 == stack->len)
	{
		stack->first->next = node;
		stack->first->prev = node;
		node->next = stack->first;
		node->prev = stack->first;
	}
	else
	{
		node->prev = stack->first->prev;
		node->prev->next = node;
		node->next = stack->first;
		stack->first->prev = node;
	}
	stack->len += 1;
	return (node);
}

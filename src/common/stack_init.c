/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   stack_init.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/04 16:21:08 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/04 16:21:08 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node			*stack_init(t_stack *stack, t_node *node)
{
	stack->first = node;
	node->next = node;
	node->prev = node;
	stack->len = 1;
	return (node);
}

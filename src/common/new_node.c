/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_node.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/03 19:30:51 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/03 19:30:51 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "common.h"

t_node	*new_node(int value)
{
	t_node	*new;

	if (NULL == (new = malloc(sizeof(t_node))))
		return (NULL);
	new->next = NULL;
	new->prev = NULL;
	new->value = value;
	new->rank = -1;
	return (new);
}

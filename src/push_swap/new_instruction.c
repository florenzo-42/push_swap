/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   new_instruction.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 20:31:20 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 20:31:20 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

t_instruction	*new_instruction(t_move move)
{
	t_instruction	*ret;

	if (NULL == (ret = malloc(sizeof(t_instruction))))
		return (NULL);
	ret->move = move;
	ret->next = NULL;
	return (ret);
}

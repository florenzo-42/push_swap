/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_setup.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 16:51:18 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 16:51:18 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		stacks_setup(t_stack *a, t_stack *b, t_seq *seq)
{
	core_sx(SETUP, a, b, seq);
	core_px(SETUP, a, b, seq);
	core_rx(SETUP, a, b, seq);
	core_rrx(SETUP, a, b, seq);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seq_print.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:28:05 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:28:05 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

/*
** TODO :
** Bufferize all output
*/

void			print_move(t_move move)
{
	if (PA == move)
		write(1, "pa\n", 3);
	else if (PB == move)
		write(1, "pb\n", 3);
	else if (SA == move)
		write(1, "sa\n", 3);
	else if (SB == move)
		write(1, "sb\n", 3);
	else if (SS == move)
		write(1, "ss\n", 3);
	else if (RA == move)
		write(1, "ra\n", 3);
	else if (RB == move)
		write(1, "rb\n", 3);
	else if (RR == move)
		write(1, "rr\n", 3);
	else if (RRA == move)
		write(1, "rra\n", 4);
	else if (RRB == move)
		write(1, "rrb\n", 4);
	else if (RRR == move)
		write(1, "rrr\n", 4);
}

void			seq_print(t_seq *seq)
{
	t_instruction	*current;

	if (0 >= seq->len)
		return ;
	current = seq->first;
	while (NULL != current->next)
	{
		print_move(current->move);
		current = current->next;
	}
	print_move(current->move);
}

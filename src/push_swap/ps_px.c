/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_px.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 20:01:04 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 20:01:04 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	core_px(int flag, t_stack *a, t_stack *b, t_seq *seq)
{
	static t_stack	*aptr;
	static t_stack	*bptr;
	static t_seq		*seqptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		seqptr = seq;
	}
	else if (FLAG_A & flag)
	{
		stack_add_first(aptr, stack_remove(bptr));
		seq_add(seqptr, PA);
	}
	else
	{
		stack_add_first(bptr, stack_remove(aptr));
		seq_add(seqptr, PB);
	}
}

void	pa(void)
{
	core_px(FLAG_A, NULL, NULL, NULL);
}

void	pb(void)
{
	core_px(FLAG_B, NULL, NULL, NULL);
}

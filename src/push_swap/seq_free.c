/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seq_free.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 13:55:53 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 13:55:53 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			seq_free(t_seq *seq)
{
	t_instruction	*current;
	t_instruction	*tmp;

	if (NULL == seq)
		return ;
	if (NULL == seq->first)
	{
		seq->len = 0;
		return ;
	}
	current = seq->first;
	while (NULL != current->next)
	{
		tmp = current;
		current = current->next;
		free(tmp);
	}
	free(current);
	seq->first = NULL;
	seq->last = NULL;
	seq->len = 0;
}

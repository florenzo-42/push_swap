/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sort.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 16:12:19 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 16:12:19 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	case_2_r(t_stack *a)
{
	if (a->first->rank < a->first->next->rank)
		sa();
}

void	case_3_r(t_stack *a)
{
	if (a->first->rank > a->first->next->rank)
	{
		if (a->first->prev->rank > a->first->rank)
			rra();
		else if (a->first->prev->rank > a->first->next->rank)
		{
			rra();
			sa();
		}
	}
	else
	{
		if (a->first->prev->rank < a->first->rank)
			sa();
		else if (a->first->prev->rank > a->first->next->rank)
		{
			ra();
			sa();
		}
		else
			ra();
	}
}

void	case_2(t_stack *a)
{
	if (a->first->rank > a->first->next->rank)
		sa();
}

void	case_3(t_stack *a)
{
	if (a->first->rank > a->first->next->rank)
	{
		if (a->first->prev->rank > a->first->rank)
			sa();
		else if (a->first->prev->rank > a->first->next->rank)
			ra();
		else
		{
			ra();
			sa();
		}
	}
	else
	{
		if (a->first->prev->rank < a->first->rank)
			rra();
		else if (a->first->prev->rank > a->first->next->rank)
			return ;
		else
		{
			rra();
			sa();
		}
	}
}

int		stacks_sort(t_stack *a, t_stack *b, t_seq *bestseq)
{
	if (is_stack_sorted(a) || 1 >= a->len)
		return (0);
	else if (2 == a->len)
		case_2(a);
	else if (3 == a->len)
		case_3(a);
	else
		return (compare_algorithms(a, b, bestseq));
	return (0);
}

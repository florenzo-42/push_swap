/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   seq_add.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 20:24:27 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 20:24:27 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void			seq_add(t_seq *seq, t_move move)
{
	t_instruction	*ins;

	if (seq->len < 0 || NULL == (ins = new_instruction(move)))
	{
		seq->len = -1;
		return ;
	}
	if (0 == seq->len)
		seq->first = ins;
	else
		seq->last->next = ins;
	seq->last = ins;
	seq->len += 1;
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_rx.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:38:26 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:38:26 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		core_rx(int flag, t_stack *a, t_stack *b, t_seq *seq)
{
	static t_stack	*aptr;
	static t_stack	*bptr;
	static t_seq		*seqptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		seqptr = seq;
		return ;
	}
	if (FLAG_A & flag)
		stack_rotate(aptr);
	if (FLAG_B & flag)
		stack_rotate(bptr);
	if ((FLAG_B & flag) && (FLAG_A & flag))
		seq_add(seqptr, RR);
	else if (FLAG_A & flag)
		seq_add(seqptr, RA);
	else if (FLAG_B & flag)
		seq_add(seqptr, RB);
}

void		ra(void)
{
	core_rx(FLAG_A, NULL, NULL, NULL);
}

void		rb(void)
{
	core_rx(FLAG_B, NULL, NULL, NULL);
}

void		rr(void)
{
	core_rx(FLAG_A | FLAG_B, NULL, NULL, NULL);
}

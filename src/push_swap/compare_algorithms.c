/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   compare_algorithms.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 19:12:21 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 19:12:21 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

/*
** Add another line with your algorithm
** The 3 fields are
** - The function name (don't forget to add it's prototype to push_swap.h)
** - A one-word string with it's name
** - A boolean saying if the algorithm resulting sequence need to be optimized
**   that is if it can generate chained contradictory instructions such as pa-pb
**   More info in the REAME
*/

t_algo		g_algorithms[] = {
	{naive_selection_sort, "naive_selection_sort", 0},
	{better_selection_sort, "better_selection_sort", 0},
	{quick_sort, "quick_sort", 1}
};

static void	swap_seqs(t_seq *x, t_seq *y)
{
	t_seq	tmp;

	tmp.len = x->len;
	tmp.first = x->first;
	tmp.last = x->last;
	x->len = y->len;
	x->first = y->first;
	x->last = y->last;
	y->len = tmp.len;
	y->first = tmp.first;
	y->last = tmp.last;
}

static int	renew_and_keep(
		t_seq *seq,
		t_stack *test_stack,
		t_seq *bestseq,
		t_stack *a)
{
	if (0 == bestseq->len || seq->len < bestseq->len)
		swap_seqs(seq, bestseq);
	seq_free(seq);
	if (test_stack->len == 0)
	{
		if (stack_dup(a, test_stack))
			return (1);
	}
	else
		stack_copy(a, test_stack);
	return (0);
}

static void	init_stack_and_seq(t_seq *seq, t_stack *test_stack)
{
	test_stack->len = 0;
	test_stack->first = NULL;
	seq->len = 0;
	seq->first = NULL;
	seq->last = NULL;
}

int			compare_algorithms(t_stack *a, t_stack *b, t_seq *bestseq)
{
	t_seq		seq;
	t_stack test_stack;
	size_t		i;

	i = 0;
	init_stack_and_seq(&seq, &test_stack);
	if (renew_and_keep(&seq, &test_stack, bestseq, a))
		return (1);
	while (i < sizeof(g_algorithms) / sizeof(g_algorithms[0]))
	{
		stacks_setup(&test_stack, b, &seq);
		g_algorithms[i].f(&test_stack, b);
		ft_dprintf(2, "%s : %i moves, ", g_algorithms[i].name, seq.len);
		if (g_algorithms[i].needs_optimization && seq.len < SAFE_RECURSION_LEN)
			seq_optimize(&seq);
		ft_dprintf(2, "after optimization : %i\n", seq.len);
		renew_and_keep(&seq, &test_stack, bestseq, a);
		i += 1;
	}
	stack_free(&test_stack);
	seq_free(&seq);
	return (0);
}

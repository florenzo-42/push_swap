/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_sx.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:47:57 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:47:57 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	core_sx(int flag, t_stack *a, t_stack *b, t_seq *seq)
{
	static t_stack	*aptr;
	static t_stack	*bptr;
	static t_seq		*seqptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		seqptr = seq;
		return ;
	}
	if (FLAG_A & flag)
		stack_swap(aptr);
	if (FLAG_B & flag)
		stack_swap(bptr);
	if ((FLAG_B & flag) && (FLAG_A & flag))
		seq_add(seqptr, SS);
	else if (FLAG_A & flag)
		seq_add(seqptr, SA);
	else if (FLAG_B & flag)
		seq_add(seqptr, SB);
}

void	sa(void)
{
	core_sx(FLAG_A, NULL, NULL, NULL);
}

void	sb(void)
{
	core_sx(FLAG_B, NULL, NULL, NULL);
}

void	ss(void)
{
	core_sx(FLAG_A | FLAG_B, NULL, NULL, NULL);
}

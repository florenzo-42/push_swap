/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick_sort.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/12 17:19:19 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/12 17:19:19 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"
#include "quick_sort.h"

static void	init_quick(t_quick *info, t_stack *a)
{
	info->blen = 0;
	info->alen = a->len;
	info->minrank = 0;
	info->maxrank = a->len - 1;
	info->topush = a->len / 2;
	info->mid = info->minrank + info->topush;
	info->pushed = 0;
	info->isthisa = 1;
	info->reclvl = 0;
	info->offset = 0;
}

void		quick_sort(t_stack *a, t_stack *b)
{
	t_quick	info;

	init_quick(&info, a);
	while (info.pushed < info.topush)
	{
		if (a->first->rank < info.mid)
		{
			pb();
			info.pushed += 1;
		}
		else
			ra();
	}
	quick_sort_a_ascending(a, b, &info);
	quick_sort_b_descending(a, b, &info);
	n_pa(info.pushed);
}

/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   naive_selection_sort.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 4242/42/42 42:42:42 by florenzo          #+#    #+#             */
/*   Updated: 4242/42/42 42:42:42 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

/*
** This is a very simple toy algorithm for demonstration purposes
** Here is a direct translation of the code :
** - Rotate through stack a until it has 2 elements left
**   Everytime it sees the smallest element it pushes it into stack b
** - Once we are done we check if we should swap the 2 elements left in a
**   All the other elements in b are sorted in decreasing order
** - Finally we push all b in a, since b is sorted in decreasing order a is
**   now sorted in increasing order
*/

/*
** Things to note on the sorting "API" :
** - Any instruction can be called with its name only and no arguments
**   ex: pa();
** - Some but not all have a n_xx(n) syntax to repeat the instruction n times
** - Look at common.h for the internal representation of stacks and nodes
**   The value field of a node corresponds to it's real value
**   The rank field corresponds to it's theorical position once sorted
**   It's not cheating but intended in the exercise
*/

void		naive_selection_sort(t_stack *a, t_stack *b)
{
	int		rank;

	rank = 0;
	while (a->len > 2)
		if (a->first->rank == rank)
		{
			pb();
			rank += 1;
		}
		else
			ra();
	if (a->first->rank > a->first->next->rank)
		sa();
	n_pa(b->len);
}

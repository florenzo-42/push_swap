/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   better_selection_sort.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 20:50:52 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 20:50:52 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

static int	st_pootn_rr(t_stack *a, int rank)
{
	int		swap;

	swap = FALSE;
	while (rank != a->first->rank)
	{
		if (rank + 1 == a->first->rank)
		{
			pb();
			swap = TRUE;
		}
		else
			rra();
	}
	pb();
	if (swap)
		sb();
	return (swap);
}

static int	st_pootn_r(t_stack *a, int rank)
{
	int		swap;

	swap = FALSE;
	while (rank != a->first->rank)
	{
		if (rank + 1 == a->first->rank)
		{
			pb();
			swap = TRUE;
		}
		else
			ra();
	}
	pb();
	if (swap)
		sb();
	return (swap);
}

static int	push_one_or_the_next(t_stack *a, int rank)
{
	t_node	*current;
	int			pos;

	current = a->first;
	pos = 0;
	while (rank != current->rank)
	{
		current = current->next;
		pos += 1;
	}
	if (pos > a->len / 2)
		return (st_pootn_rr(a, rank));
	else
		return (st_pootn_r(a, rank));
}

void		better_selection_sort(t_stack *a, t_stack *b)
{
	int		min;

	min = 0;
	while (3 < a->len)
		min += 1 + push_one_or_the_next(a, min);
	if (3 == a->len)
		case_3(a);
	else
		case_2(a);
	while (0 < b->len)
		pa();
}

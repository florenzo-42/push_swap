/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   n_xx.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/14 19:19:18 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/14 19:19:18 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void		n_pa(int n)
{
	while (n-- > 0)
		pa();
}

void		n_pb(int n)
{
	while (n-- > 0)
		pb();
}

void		n_rrb(int n)
{
	while (n-- > 0)
		rrb();
}

void		n_rra(int n)
{
	while (n-- > 0)
		rra();
}

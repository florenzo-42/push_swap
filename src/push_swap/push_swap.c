/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 14:20:18 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 14:20:18 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

int					main(int ac, char **av)
{
	t_stack		a;
	t_stack		b;
	t_seq			bestseq;

	if (ac < 2)
		return (0);
	if (0 >= build_stack(&a, params_to_iarray(ac, av)))
	{
		ft_dprintf(2, "Error\n");
		return (-1);
	}
	bestseq.len = 0;
	bestseq.first = NULL;
	b.len = 0;
	b.first = 0;
	stacks_setup(&a, &b, &bestseq);
	stacks_sort(&a, &b, &bestseq);
	seq_print(&bestseq);
	seq_free(&bestseq);
	stack_free(&a);
	stack_free(&b);
	return (0);
}

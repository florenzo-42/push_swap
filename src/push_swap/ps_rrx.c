/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ps_rrx.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/10 11:45:58 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/10 11:45:58 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "push_swap.h"

void	core_rrx(int flag, t_stack *a, t_stack *b, t_seq *seq)
{
	static t_stack	*aptr;
	static t_stack	*bptr;
	static t_seq		*seqptr;

	if (SETUP & flag)
	{
		aptr = a;
		bptr = b;
		seqptr = seq;
		return ;
	}
	if (FLAG_A & flag)
		stack_reverse_rotate(aptr);
	if (FLAG_B & flag)
		stack_reverse_rotate(bptr);
	if ((FLAG_B & flag) && (FLAG_A & flag))
		seq_add(seqptr, RRR);
	else if (FLAG_A & flag)
		seq_add(seqptr, RRA);
	else if (FLAG_B & flag)
		seq_add(seqptr, RRB);
}

void	rra(void)
{
	core_rrx(FLAG_A, NULL, NULL, NULL);
}

void	rrb(void)
{
	core_rrx(FLAG_B, NULL, NULL, NULL);
}

void	rrr(void)
{
	core_rrx(FLAG_A | FLAG_B, NULL, NULL, NULL);
}

## Overview
This 42 project serves as an introductoin to sorting algorithms and the notion of complexity. The goal is to design an algorithm capable of sorting a stack "a" of unique integers, using a temporary stack "b", with a limited predefined instruction set.  

### Instruction set
| Instruction | Mirror    | Combo  | Action                                                           |
|:-----------:|:----------|:------:|:-----------------------------------------------------------------|
| (stack a)   | (stack b) | (both) |                                                                  |
| pa          | pb        |        | **Push** the element at the top of stack a to the top of stack b |
| ra          | rb        | rr     | **Rotate** stack a, the first element becomes the last           |
| rra         | rrb       | rrr    | **Reverse Rotate** stack a, the last element becomes first       |
| sa          | sb        | ss     | **Swap** stack a first and second elements                       |
### The push_swap program
`./push_swap [NUMBERS]...`  
This program takes an array of numbers as argument, representing the initial state of stack a, it then outputs a series of instructions from the set that sort the stack in ascending order (b should be empty at the end).
### The checker program
`./checker [NUMBERS]... < [sequence]`  
This program checks wether the instruction sequence read on stdin correctly sorts the numbers given as parameters. You can manually interact with the program or you can directly pipe in `push_swap` output.
## Examples
Here is how the checker program reacts to user input (i put comments starting with `#`) :
```
$> ./checker -v 2 5 1 4 3 0     # -v to show stacks between each move
stack a : 2 5 1 4 3 0
stack b : empty stack
rra                      # "shifting right"
stack a : 0 2 5 1 4 3
stack b : empty stack
pb                       # pushing 0 from a to b
stack a : 2 5 1 4 3
stack b : 0
pb
stack a : 5 1 4 3
stack b : 2 0
ra                       # "shifting right"
stack a : 1 4 3 5
stack b : 2 0
pb
stack a : 4 3 5
stack b : 1 2 0
ss                       # swapping the 2 first elements of both stacks
stack a : 3 4 5
stack b : 2 1 0
pa                       # pushing everything back into a
stack a : 2 3 4 5
stack b : 1 0
pa
stack a : 1 2 3 4 5
stack b : 0
pa
stack a : 0 1 2 3 4 5
stack b : empty stack
^D                       # ending user input
OK                       # the sequence correctly sorted the stack
```
You can also test the push_swap program with large inputs and see how the algorithms behave.  
To generate random numbers i recommend using GNU `shuf` but you can also do that with the scripting language of your choice.  
With zsh and shuf :
```
$> NUMS=(`shuf -i 1-100`); ./push_swap $NUMS 2> /dev/null | ./checker -n $NUMS
OK                   # The algorithm is correct
Played 787 moves     # -n displays final number of moves
```
## Implementation details
### Stacks
I represented the 2 stacks using doubly linked lists with a control structure so that every instruction is O(1) and with constant memory. The list creation should be refactored, i am currentyle `malloc`ating every node individually while i could simply allocate an array of structures as i know it's size since the beginning.

### Performance comparison
If you run push_swap without redirecting `stderr` you will see something like this
```
$> ./push_swap `shuf -i 1-100` > /dev/null
naive_selection_sort : 2609 moves, after optimization : 2609
better_selection_sort : 1367 moves, after optimization : 1367
quick_sort : 1030 moves, after optimization : 764
```
Actually i am using a function pointer array so that i can "plug in" as many different algorithms as i want (you can see below if you want to add yours). I am echoing this so that i can generate graphs showing the performance of every algorithm thanks to [this python script](graph.py "graph.py").  

![graph](graph.png)  

Here we can see that the selection sort algorithms run in O(n^2) time, the "better" one simply has a lower constant coefficient, they quickly become unusable on large inputs. The quick sort algorithm instead has an O(n lg(n)) time complexity.
### Sequence optimization
I keep all the sequence of instructions in a singly linked list, the "optimization" part is when i remove all the redundant chained instructions such as `pa`-`pb` (which consumes 2 moves for nothing), i only do this for the quick_sort which i know generates a lot of them, the 2 other algorithms don't need this kind of optimization.

While doing this i found an interesting programming problem, imagine the following sequence of instructions representing nodes on a singly linked list :  
```
sb -> pb -> ra -> pb -> pa -> rra -> pa -> rr
      │     │     └─────┘     │      │
      │     └─────────────────┘      ├──── All these should be removed, but how ?
      └──────────────────────────────┘
```
I implemented a straightforward recursive algorithm that does that in linear time [here](src/push_swap/seq_optimize.c), however it also has an O(n) space complexity and can create a stack overflow on really big inputs, i would be curious to see an iterative solution that still has the same performance.
### Adding your own algorithm(s)
In the end this project has become an algorithm testing framework. I would be very curious to see other people's algorithms and how they compare so here is a guide on how to create your own.
* Clone the repository `git clone --recursive http://gitlab.com/florenzo-42/push_swap`
* Add your algorithm `myalgorithm.c` in `src/push_swap/algorithms/` (the folder is important)
* Add it's prototype (`void myalgorithm(t_stack *a, t_stack *b);`) in the push_swap header ([`inc/push_swap.h`](inc/push_swap.h)).
* Build the project with `make`
* Generate graph with `./graph.py 500` (requires python 3.5+ and `shuf`)
* Send pull request :heart:

I did some wrapper functions that allow you to code comfortably, look at the example algorithm [`src/push_swap/algorithms/naive_selection_sort.c`](src/push_swap/algorithms/naive_selection_sort.c).

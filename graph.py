#!/usr/bin/env python3
import matplotlib.pyplot as plt
import subprocess
import shutil
import sys

assert sys.version_info >= (3,5)

if len(sys.argv) != 2:
    print('usage : ./graph.py n')
    print('        where n is the maximal number of arguments to test')
    sys.exit()

try:
    end = int(sys.argv[1])
except:
    print("'%s' isn't a valid number")
    sys.exit()

if not shutil.which('shuf'):
    print("you don't seem to have shuf installed, edit the script to use something else")
    sys.exit()

start = 10
data= []
xs = range(start, end + 10, 10)
for x in xs:
    result = subprocess.run('./push_swap `shuf -i 1-%d` > /dev/null' % x,
                            stderr = subprocess.PIPE,
                            universal_newlines = True,
                            shell = True)
    lines = result.stderr.splitlines()
    for line in lines:
        words = line.split()
        #      algorithm name, optimized moves
        data.append([words[0], int(words[7])])

# An array with the names of the algorithms
algorithms = []
for x in data:
    if x[0] not in algorithms:
        algorithms.append(x[0])
    else:
        break

for algo in algorithms:
    ys = [x[1] for x in data if x[0] == algo]
    plt.plot(xs, ys, label = algo)

plt.axis([start, end, 0, None])
plt.xlabel('Numbers')
plt.ylabel('Moves')
plt.legend()
plt.show()

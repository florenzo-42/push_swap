/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 16:46:54 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 16:46:54 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PUSH_SWAP_H
# define PUSH_SWAP_H
# include "common.h"
# define SAFE_RECURSION_LEN 42000

typedef enum	e_move
{
	PA,
	PB,
	SA,
	SB,
	SS,
	RA,
	RB,
	RR,
	RRA,
	RRB,
	RRR,
}				t_move;

typedef struct	s_instruction
{
	struct s_instruction	*next;
	t_move					move;
}				t_instruction;

typedef struct	s_seq
{
	t_instruction	*first;
	t_instruction	*last;
	int				len;
}				t_seq;

/*
** Sequences and Instructions
*/

t_instruction	*new_instruction(t_move move);
void			seq_print(t_seq *seq);
void			seq_add(t_seq *seq, t_move move);
void			print_move(t_move move);
void			seq_free(t_seq *seq);
int				stack_dup(t_stack *orig, t_stack *new);

/*
** Sorting algorithms
*/

int				stacks_sort(t_stack *a, t_stack *b, t_seq *bestseq);
void			naive_selection_sort(t_stack *a, t_stack *b);
void			better_selection_sort(t_stack *a, t_stack *b);
void			quick_sort(t_stack *a, t_stack *b);
int				compare_algorithms(t_stack *a, t_stack *b,
		t_seq *bestseq);
void			seq_optimize(t_seq *seq);

typedef struct	s_algo
{
	void		(*f)(t_stack*, t_stack*);
	char		*name;
	int			needs_optimization;
}				t_algo;

/*
** Hardcoded small cases
*/

void			case_2(t_stack *a);
void			case_2_r(t_stack *a);
void			case_3(t_stack *a);
void			case_3_r(t_stack *a);

/*
** Core functions
*/

void			core_px(int flag, t_stack *a, t_stack *b, t_seq *seq);
void			core_rrx(int flag, t_stack *a, t_stack *b, t_seq *seq);
void			core_rx(int flag, t_stack *a, t_stack *b, t_seq *seq);
void			core_sx(int flag, t_stack *a, t_stack *b, t_seq *seq);
void			stacks_setup(t_stack *a, t_stack *b, t_seq *seq);

/*
** Easier instructions
*/

void			n_pa(int n);
void			n_pb(int n);
void			n_rra(int n);
void			n_rrb(int n);

#endif

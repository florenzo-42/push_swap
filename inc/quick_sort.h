/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   quick_sort.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <florenzo@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 4242/42/42 42:42:42 by florenzo          #+#    #+#             */
/*   Updated: 4242/42/42 42:42:42 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef QUICK_SORT_H
# define QUICK_SORT_H
# include "push_swap.h"

typedef struct	s_quick
{
	int		alen;
	int		blen;
	int		minrank;
	int		maxrank;
	int		topush;
	int		pushed;
	int		mid;
	int		isthisa;
	int		reclvl;
	int		offset;
}				t_quick;

void			quick_sort_a_ascending(t_stack *a, t_stack *b,
		t_quick *data);
void			quick_sort_b_descending(t_stack *a, t_stack *b,
		t_quick *data);

#endif

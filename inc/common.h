/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   push_swap.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/01 17:37:00 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/01 17:37:00 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef COMMON_H
# define COMMON_H
# include <stdlib.h>
# include <unistd.h>
# include "printf.h"
# include "libft.h"
# define RESET "\033[0m"
# define SETUP 1
# define FLAG_A 2
# define FLAG_B 4

typedef struct		s_node
{
	struct s_node	*next;
	struct s_node	*prev;
	int					value;
	int					rank;
}					t_node;

typedef struct		s_stack
{
	t_node	*first;
	int			len;
}					t_stack;

typedef struct		s_iarray
{
	int			*array;
	int			size;
}					t_iarray;

/*
** Stack manipulation
*/

int					build_stack(t_stack *stack, t_iarray *iarr);
t_node			*new_node(int value);
t_node			*stack_add_end(t_stack *stack, t_node *node);
t_node			*stack_add_first(t_stack *stack, t_node *node);
void				stack_free(t_stack *stack);
t_node			*stack_init(t_stack *stack, t_node *node);
void				stack_print(t_stack *stack, char name,
		char *color, int index);
t_node			*stack_remove(t_stack *stack);
void				stack_rotate(t_stack *stack);
void				stack_reverse_rotate(t_stack *stack);
void				stack_swap(t_stack *stack);
int					is_stack_sorted(t_stack *stack);
void				stack_copy(t_stack *orig, t_stack *new);
t_node			*node_copy(t_node *orig);

/*
** Int arrays
*/

t_iarray			*iarr_new(int const size);
void				iarr_free(t_iarray *iarr);
t_iarray			*iarr_copy(t_iarray *iarr);
t_iarray			*params_to_iarray(int ac, char **av);
t_iarray			*iarr_check_doubles(t_iarray *iarr);
int					iarr_bsearch(int n, t_iarray *iarr, int growing);

/*
** quick functions (static variables black magic)
*/

void				pa(void);
void				pb(void);
void				sa(void);
void				sb(void);
void				ss(void);
void				ra(void);
void				rb(void);
void				rr(void);
void				rra(void);
void				rrb(void);
void				rrr(void);

#endif

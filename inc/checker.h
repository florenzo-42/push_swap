/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   checker.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: florenzo <lorenzo.farnetani@student.42.fr> +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/02/09 16:24:02 by florenzo          #+#    #+#             */
/*   Updated: 2018/02/09 16:24:02 by florenzo         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CHECKER_H
# define CHECKER_H
# include "common.h"

/*
** Structures
*/

typedef struct		s_flags
{
	char			*acolor;
	char			*bcolor;
	int				verbose;
	int				colors;
	int				counter;
	int				count;
	int				error;
	int				index;
	int				repeat;
}					t_flags;

/*
** Flags
*/

void				ft_usage(void);
void				ft_help(void);
void				short_arg(char *str, t_flags *flags);
void				long_arg(char *str, t_flags *flags);
void				get_flags(int *ac, char ***av, t_flags *flags);
void				chk_correct_flags(t_flags *flags);

/*
** Checker
*/

int					chk_handle_input(char *str);
void				show_stacks(t_stack *a, t_stack *b,
		t_flags *flags);

/*
** core functions
*/

void				stacks_setup(t_stack *a, t_stack *b);
void				core_px(int flag, t_stack *a, t_stack *b);
void				core_sx(int flag, t_stack *a, t_stack *b);
void				core_rx(int flag, t_stack *a, t_stack *b);
void				core_rrx(int flag, t_stack *a, t_stack *b);

#endif
